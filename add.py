import webapp2
import jinja2
import os
from google.appengine.ext import ndb
from google.appengine.api import users
from myuser import MyUser1
from anagram import Anagram


JINJA_ENVIRONMENT = jinja2.Environment(
loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
extensions=['jinja2.ext.autoescape'],
autoescape=True
)

class AddAnagram(webapp2.RequestHandler):
	def get(self):

		template = JINJA_ENVIRONMENT.get_template('add.html')
		self.response.write(template.render())
		# self.redirect('/')


	def post(self):
		
		test=''
		switch = self.request.get('button1')

		#Add new word
		if switch == 'Add':
			test='Add'
			word = self.request.get('input_word').lower()
			
			if word:
				key_word = list(word)
				
				#prepare word for key name
				key_word = self.letter_sort(self,key_word)
				letter_count = len(key_word)
				key_word = ''.join(key_word)

				#get id of current user for key
				user = users.get_current_user()
				key_name = user.user_id() + key_word

				
				new_word_key = ndb.Key('Anagram', key_name)
				new_word = new_word_key.get()

				if new_word== None:
					new_word = Anagram(id= key_name, count=1, letter_count=letter_count)
					new_word.anagram_list.append(word)
					new_word.put()

				else:
					if word not in new_word.anagram_list:
						new_word.count+=1
						new_word.anagram_list.append(word)
						new_word.put()


		#Upload using File
		elif switch=='Upload':
			test = self.request.get('file_1')
			with open(test) as f:
			
				lines = [line.rstrip('\n') for line in open(test)]
			
			for x in lines:

			
				if x:
					word = x.lower()
					key_word = list(word)
					
					#prepare word for key name
					key_word = self.letter_sort(self,key_word)
					letter_count = len(key_word)
					key_word = ''.join(key_word)

					#get id of current user for key
					user = users.get_current_user()
					key_name = user.user_id() + key_word

					
					new_word_key = ndb.Key('Anagram', key_name)
					new_word = new_word_key.get()

					if new_word== None:
						new_word = Anagram(id= key_name, count=1, letter_count=letter_count)
						new_word.anagram_list.append(word)
						new_word.put()

					else:
						if word not in new_word.anagram_list:
							new_word.count+=1
							new_word.anagram_list.append(word)
							new_word.put()
			
			

		# debug={'test': test}
		# template = JINJA_ENVIRONMENT.get_template('debugging.html')
		# self.response.write(template.render(debug))
		self.redirect('/')

  	@staticmethod
	def letter_sort(self, word):
		#returns a list of letters sorted lexographically
		word.sort()
		return word