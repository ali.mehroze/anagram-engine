from google.appengine.ext import ndb

class Anagram(ndb.Model):
	anagram_list = ndb.StringProperty(repeated=True)
	count = ndb.IntegerProperty()
	letter_count = ndb.IntegerProperty()