import webapp2
import jinja2
from google.appengine.api import users
import os
from google.appengine.ext import ndb
from myuser import MyUser1
from anagram import Anagram
from add import AddAnagram
from subanagram import SearchSubAnagram



JINJA_ENVIRONMENT = jinja2.Environment(
loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
extensions=['jinja2.ext.autoescape'],
autoescape=True
)
JINJA_ENVIRONMENT.globals['uri_for'] = webapp2.uri_for

class MainPage(webapp2.RequestHandler):
	def get(self):
		self.response.headers['Content-Type'] = 'text/html'
		# URL that will contain a login or logout link
		url = ''
		url_string = ''
		welcome_message = 'Welcome back'
		# pull the current user from the request
		user = users.get_current_user()
		template_debug = ''
		word_list2=''

		if user:
			url= users.create_logout_url(self.request.uri)
			url_string = 'logout'

			myuser_key = ndb.Key('MyUser1',user.user_id())
			myuser = myuser_key.get()

			if myuser == None:
				welcome_message = 'Welcome to the application'

				myuser = MyUser1(id = user.user_id())
				template_debug = myuser.put()


			else:
				
				display_string=''
				word_list = Anagram.query().fetch()
				results=[]
				search_result_final=[]
				anagram_count=0
				word_count=0
				
				if self.request.get('button')=='Search':
					display_string='Search Results:'
					search_anagram = self.request.get('search_anagram')
					search_anagram = list(search_anagram)
					
					#prepare word for key name
					search_anagram = AddAnagram.letter_sort(self,search_anagram)
					search_anagram = ''.join(search_anagram)

					search_anagram = user.user_id()+search_anagram
					
					result_keys = ndb.Key('Anagram', search_anagram)
					search_results = result_keys.get()
					
					if search_results!=None:

						search_result_final=search_results
					else:
						return self.redirect('/')

					
				elif self.request.get('button')=='View All' :

					if word_list!=None:
						display_string='All Results'
						
						for x in word_list:
							if user.user_id() in x.key.id():
								results.append(x)
								anagram_count+=1
								word_count+=x.count
			
		else:
			url = users.create_login_url(self.request.uri)
			url_string = 'Login'
		
		template_values ={
			'url':url,
			'url_string': url_string,
			'display_string': display_string,
			'user': user,
			'welcome_message':welcome_message,
			'search_result_final':search_result_final,
			'template_debug': results,
			'anagram_count':anagram_count,
			'word_count':word_count

		}

		
		# pull the template file and ask jinja to render
		# it with the given template values
		template = JINJA_ENVIRONMENT.get_template('main.html')
		self.response.write(template.render(template_values))

	


app = webapp2.WSGIApplication([webapp2.Route(r'/', handler=MainPage, name = 'mainpage'),
	webapp2.Route(r'/add', handler=AddAnagram, name = 'add'),
	webapp2.Route(r'/subanagram', handler=SearchSubAnagram, name = 'subanagram')],
	debug=True)