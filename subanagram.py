import webapp2
import jinja2
import os
from google.appengine.ext import ndb
from google.appengine.api import users
from myuser import MyUser1
from anagram import Anagram
from add import AddAnagram
from itertools import combinations


JINJA_ENVIRONMENT = jinja2.Environment(
loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
extensions=['jinja2.ext.autoescape'],
autoescape=True
)


class SearchSubAnagram(webapp2.RequestHandler):
	def subanagram_search(self,word):
	    sub_keys = []

	    for i in range(3, len(word)+1):
	    	readword = (["".join(c) for c in combinations(word,i)])
	        
	        for x in readword:
	            sub_keys.append(x)

	    return sub_keys

	def get(self):

		template = JINJA_ENVIRONMENT.get_template('subanagram.html')
		self.response.write(template.render())


	def post(self):
		search_anagram = self.request.get('input_subanagram')
		search_anagram = list(search_anagram)
		user = users.get_current_user()	
		result_list = []
		
		#prepare word for key name
		search_anagram = AddAnagram.letter_sort(self,search_anagram)
		
		search_anagram = ''.join(search_anagram)

		subanagram_list = self.subanagram_search(search_anagram)

		for x in subanagram_list:
			search_subanagram_key = ndb.Key('Anagram', user.user_id() + x )
			search_subanagram = search_subanagram_key.get()

			if search_subanagram != None:
				result_list.append(search_subanagram.anagram_list)

		testing_values = {'test': result_list}

		template = JINJA_ENVIRONMENT.get_template('subanagram.html')
		self.response.write(template.render(testing_values))


	